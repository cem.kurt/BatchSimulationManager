import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import ClusterScreen from './Components/Screens/ClusterScreen';
import DatabaseScreen from './Components/Screens/DatabaseScreen';
import MeasurementsScreen from './Components/Screens/MeasurementsScreen'
import ScenerioAttributesScreen from './Components/Screens/ScenerioAttributesScreen'
import ScenerioScreen from './Components/Screens/ScenerioScreen'
//hello from jira
const CONTENT = {
  tableHead: ["-", "Variable Name", "Variable Details", "Variable Range"],
  tableTitle: [
    "1",
    "2",
    "1",
    "2",
    "1",
    "2",
    "1 ",
    "1",
    "2",
    "1",
    "2",
    "1",
    "2",
    "1 ",
  ],
  tableData: [
    ["1", "2", "3"],
    ["a", "b", "c"],
    ["a", "b", "c"],
    ["a", "b", "c"],
    ["a", "b", "c"],
    ["a", "b", "c"],
    ["a", "b", "c"],
    ["1", "2", "3"],
    ["a", "b", "c"],
    ["a", "b", "c"],
    ["a", "b", "c"],
    ["a", "b", "c"],
    ["a", "b", "c"],
    ["a", "b", "c"],
  ],
};

function ScenerioScreenHandler() {
  return (
    <ScenerioScreen CONTENT = {CONTENT}/>
  );
}

function ScenerioAttributesScreenHandler() {
  return (
    <ScenerioAttributesScreen/>
  );
}

function MeasurementsScreenHandler() {
  return (
    <MeasurementsScreen/>
  );
}

function DatabaseScreenHandler() {
  return (
    <DatabaseScreen/>
  );
}

function ClusterScreenHandler() {
  return (
    <ClusterScreen/>
  );
}

const Tab = createMaterialTopTabNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Tab.Navigator>
        <Tab.Screen name="Scenerio" component={ScenerioScreenHandler} />
        <Tab.Screen name="Scenerio Attributes" component={ScenerioAttributesScreenHandler} />
        <Tab.Screen name="Measurements" component={MeasurementsScreenHandler} />
        <Tab.Screen name="Database" component={DatabaseScreenHandler} />
        <Tab.Screen name="Cluster" component={ClusterScreenHandler} />
      </Tab.Navigator>
    </NavigationContainer>
  );
}
