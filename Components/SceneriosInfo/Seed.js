import { View, Text, TextInput, StyleSheet } from "react-native-web"


function Seed() {
    return(
        <View>
            <View>
                <Text>Unique Name</Text><TextInput/>
                <Text>Initial Value</Text><TextInput/>
            </View>
            <View>
                <Text>Increment</Text><TextInput/>
                <Text>Final Value</Text><TextInput/>
            </View>
        </View>
    )
}

export default Seed