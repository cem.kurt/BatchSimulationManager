import { ScrollView, View, StyleSheet } from "react-native-web";
import {
  Table,
  TableWrapper,
  Row,
  Rows,
  Col,
} from "react-native-table-component";

function TableView({ CONTENT }) {
  const ScrollviewStickyHeader = () => {
    return (
      <Row
        data={CONTENT.tableHead}
        flexArr={[1, 2, 2, 2]}
        style={styles.head}
        textStyle={styles.sceneriosText}
      />
    );
  };

  return (
    <View style={styles.sceneriosContainer}>
      <ScrollviewStickyHeader />

      <ScrollView
        persistentScrollbar={true}
        stickyHeaderHiddenOnScroll={true}
        stickyHeaderIndices={[1]}
        style={styles.scrollView}
      >
        <Table>
          <TableWrapper style={styles.wrapper}>
            <Col
              data={CONTENT.tableTitle}
              style={styles.sceneriosTitle}
              heightArr={[28, 28]}
              textStyle={styles.sceneriosText}
            />
            <Rows
              data={CONTENT.tableData}
              flexArr={[2, 2, 2]}
              style={styles.row}
              textStyle={styles.sceneriosText}
            />
          </TableWrapper>
        </Table>
      </ScrollView>
    </View>
  );
}

export default TableView;

const styles = StyleSheet.create({
  sceneriosContainer: {
    
    height: 300,
    shadowColor: "black",
    borderRadius: 6,
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.7,
    shadowRadius: 6,
    borderBottomWidth: 1,
  },

  head: {
    height: 40,
    width: 600,
    backgroundColor: "orange",
    borderBottomWidth: 1,
    borderRadius: 2,
  },
  wrapper: { flexDirection: "row" },
  sceneriosTitle: { flex: 1, backgroundColor: "#2ecc71" },
  row: { height: 28 },
  sceneriosText: { textAlign: "center" },
  scrollView: { borderBottomLeftRadius: 6 },
});
