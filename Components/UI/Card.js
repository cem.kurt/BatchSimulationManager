import { View, StyleSheet } from "react-native-web";

function Card({children}) {
    return(
        <View style = {styles.card}>
            {children}
        </View>
    )
}

export default Card

const styles = StyleSheet.create({
    card: {
        alignItems:'center',
        adjustItems:'center',   
        textAlign:'center',
        borderWidth:1,
        borderRadius: 6,
        shadowColor: 'gray',
        shadowOffset: {width: 2, height: 1},
        shadowOpacity: 0.5,
        padding: 8,
    }
})