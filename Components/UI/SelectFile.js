import React, { useCallback, useState } from "react";
import { useDropzone } from "react-dropzone";
import { Pressable, StyleSheet, View, Text } from "react-native-web";

function SelectFile({ children }) {
  const onDrop = useCallback((acceptedFiles) => {
    setGetFileProps(acceptedFiles[0].name);
  }, []);
  const { getRootProps, getInputProps, isDragActive } = useDropzone({ onDrop });
  const [getFileProps, setGetFileProps] = useState(null);

  return (
    <View style = {styles.container}>
      <Pressable
        style={({ pressed, hovered }) => [
          styles.chooseButtons,
          getFileProps ? styles.fileSelected : styles.chooseButtons,
          pressed && styles.pressablePressed,
          hovered && styles.pressableHovered,
        ]}
      >
        <View style = {styles.selectFileContainer}>
          <View style={styles.input} {...getRootProps()}>
            <input {...getInputProps()} />
            {isDragActive ? <p>Drop the files here ...</p> : <p>{children}</p>}
            {console.log(getInputProps.name.toString())}
          </View>
          {getFileProps && <Text>{getFileProps}</Text>}
        </View>
      </Pressable>
    </View>
  );
}

export default SelectFile;

const styles = StyleSheet.create({
  container: {
    flex:1,
    paddingHorizontal:8
    
  },
  selectFileContainer: {
    flexDirection: "row",
    maxHeight: 20,
    marginHorizontal: 16,
    alignItems: "center",
    adjustItems: "center",
  },
  input: {
    marginHorizontal: 2,
  },
  fileSelected:{
    paddingHorizontal: 2,
    opacity: 0.5,
    borderWidth: 1,
    borderRadius: 6,
    maxHeight: 20,
    borderColor: "gray",
    shadowColor: "gray",
    shadowOffset: { width: 1, height: 2 },
    shadowOpacity: 0.5,
    alignItems: "center",
    adjustItems: "center",
    shadowRadius: 6,
    flexDirection: "row",
    backgroundColor: "#2ecc71"
  },
  chooseButtons: {
    paddingHorizontal: 2,

    borderRadius: 6,
    maxHeight: 20,
    borderColor: "gray",
    shadowColor: "gray",
    shadowOffset: { width: 1, height: 2 },
    shadowOpacity: 0.5,
    alignItems: "center",
    adjustItems: "center",
    shadowRadius: 6,
    flexDirection: "row",
  },
  pressablePressed: {
    paddingHorizontal: 2,
    maxHeight: 20,
    borderColor: "gray",
    shadowColor: "gray",
    shadowOffset: { width: 1, height: 2 },
    shadowOpacity: 0.5,
    alignItems: "center",
    adjustItems: "center",
    shadowRadius: 6,
    flexDirection: "row",
    backgroundColor: "rgb(210, 230, 255)",
  },
  pressableHovered: {
    paddingHorizontal: 2,
    borderWidth: 1,
    borderRadius: 6,
    maxHeight: 20,
    borderColor: "gray",
    shadowColor: "gray",
    shadowOffset: { width: 1, height: 2 },
    shadowOpacity: 0.5,
    alignItems: "center",
    adjustItems: "center",
    shadowRadius: 6,
    flexDirection: "row",
    opacity: 0.5,
  },
  chooseFileText:{
    fontSize: 20,
   
  }
});
