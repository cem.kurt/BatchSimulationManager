import { Pressable, View, StyleSheet, Text, Animated } from "react-native-web";

function PressableButton({ children, style }) {
  function buttonPressed() {
    console.log("button pressed");
  }
  const animatedButtonScale = new Animated.Value(1);

    // When button is pressed in, animate the scale to 1.5
    const onPressIn = () => {
        Animated.spring(animatedButtonScale, {
            toValue: 0.9,
            useNativeDriver: true,
        }).start();
    };

    // When button is pressed out, animate the scale back to 1
    const onPressOut = () => {
        Animated.spring(animatedButtonScale, {
            toValue: 1,
            useNativeDriver: true,
        }).start();
    };

    // The animated style for scaling the button within the Animated.View
    const animatedScaleStyle = {
        transform: [{scale: animatedButtonScale}]
    };

  return (
    <Pressable onPress={buttonPressed} 
        onPressIn = {onPressIn}
        onPressOut = {onPressOut}
        
        style={({ pressed, hovered }) => [
            pressed && styles.buttonPressed,
            hovered && styles.buttonHovered,
        ]}>
      <Animated.View style={[styles.container, style, animatedScaleStyle]}>
        <Text style={styles.text}>{children}</Text>
      </Animated.View>
    </Pressable>
  );
}

export default PressableButton;

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    borderWidth: 1,
    borderRadius: 6,
    borderColor: "transparent",
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 6,
    shadowOpacity: 0.3,
    shadowColor: "black",
    backgroundColor: "#2ecc71",
    padding: 4,
  },
  text: {
    textAlign: "center",
    color: "white",
  },
  buttonPressed:{
    
  },
  buttonHovered:{
    opacity:0.7
  }
});
