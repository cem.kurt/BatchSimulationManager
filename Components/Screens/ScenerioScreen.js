import { View, StyleSheet, Text, Button } from "react-native-web";
import SelectFile from "../UI/SelectFile";
import { useState } from "react";
import * as Progress from "react-native-progress";
import TableView from "../UI/TableView";
import SelectList from "react-native-dropdown-select-list";
import PressableButton from "../UI/PressableButton";
import Seed from "../SceneriosInfo/Seed";

let scenerioInfoScreen
function ScenerioScreen({ CONTENT }) {
  let tableContent = CONTENT;
  const [selected, setSelected] = useState("");

  const dropdownData = [
    { key: "1", value: "Seed" },
    { key: "2", value: "Radar Emmission Power" },
    { key: "3", value: "Weapon Range" },
    { key: "4", value: "Radar Range" },
    { key: "5", value: "Loaded Weapon Count" }
  ];
  if(selected === "1"){
    scenerioInfoScreen = <Seed/>
  }


  return (
    <View style={styles.container}>
      <View style={styles.chooseButtonsContainer}>
        <View style={styles.chooseDataBase}>
          <SelectFile>Choose Database: </SelectFile>
        </View>
        <View style={styles.chooseScenerio}>
          <SelectFile>Choose Scenerio: </SelectFile>
        </View>
      </View>
      <TableView CONTENT={tableContent} />
      <View style={styles.progressContainer}>
        <Progress.Bar
          progress={0.3}
          
          width={580}
          color={"#2ecc71"}
          height={18}
          style={styles.progressBar}
        />
        <Text>30%</Text>
      </View>
      <View style={styles.scenerioFooterContainer}>
        <View style={styles.etDBElement}>
          <PressableButton>ET DB ELEMENT</PressableButton>
        </View>
        <SelectList
          boxStyles={styles.selectList}
          setSelected={setSelected}
          dropdownStyles={styles.dropdown}
          data={dropdownData}
          onSelect={() => alert(selected)}
        />
        <View style={styles.addVariable}>
          <PressableButton>Add Variable</PressableButton>
        </View>
        <View stlye = {[styles.deleteVariable]}>
          <PressableButton style = {styles.stopColor}>Delete Variable</PressableButton>
        </View>
      </View>
      <View style={styles.startButtons}>
        <PressableButton style={[styles.bottomButtons, styles.stopColor]}>Stop</PressableButton>
        <PressableButton style={styles.bottomButtons}>Run</PressableButton>
        <PressableButton style={styles.bottomButtons}>Continue</PressableButton>
      </View>
    </View>
  );
}

export default ScenerioScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
  },
  chooseButtonsContainer: {
    paddingTop: 12,
    alignItems: "center",
  },
  chooseDataBase: {
    alignItems: "flex-start",
    paddingVertical: 8,
  },
  chooseScenerio: {
    paddingBottom: 16,
    paddingTop: 8,
  },
  progressContainer: {
    flexDirection: "row",
    alignItems: "center",
    adjustItems: "center",
  },
  progressBar: {
    marginVertical: 20,
    marginRight: 4,
    borderRadius: 6,
  },
  scenerioFooterContainer: {
    alignItems: "center",
    flexDirection: "row",
  },
  addVariable: {
    marginHorizontal: 8,
    flexDirection: "row",
    marginRight: 8,
  },
  etDBElement:{
    marginHorizontal: 8,
    flexDirection: "row",
    marginRight: 140,
  },
  deleteVariable: {
    marginHorizontal: 8,
    flexDirection: "row",
    marginRight: 8,
  },
  selectList: {
    shadowOpacity:0.5,
    shadowRadius:6,
    borderWidth:0.2,
    adjustItems:"center",
    alignItems: "center",
    textAlign: "center",
    maxHeight: 30,
  },
  startButtons:{
    marginHorizontal: 8,
    flexDirection: "row",
    marginLeft: 350,
    marginTop:16,
    zIndex:-5
  },
  bottomButtons:{
    marginHorizontal:8,
    width: 75
  },
  stopColor:{
    backgroundColor: "#ff424f"
  },
  dropdown:{
    position:"absolute",
    backgroundColor: "#f2f2f2",
    position: "fixed" 
  }
});
